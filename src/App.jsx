import MainContent from './components/MainContent';
import logo from './assets/images/logo.png';
import reactLogo from './assets/images/react-logo.svg';
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={reactLogo} className="App-logo" alt="logo" />
        <p>
          This is example application for demonstrating GitLab CI/CD with React.js web app.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <p>MM</p>
      </header>
      <MainContent />
    </div>
  );
}

export default App;
